import {Component, OnDestroy, OnInit} from '@angular/core';
import {FaceSnap} from "../../../core/models/face-snap.models";
import {FaceSnapService} from "../../../core/services/face-snap.service";
import {interval, Observable, Subject, takeUntil, tap} from "rxjs";

@Component({
  selector: 'app-face-snap-list',
  templateUrl: './face-snap-list.component.html',
  styleUrls: ['./face-snap-list.component.scss']
})
export class FaceSnapListComponent implements OnInit, OnDestroy{

  faceSnaps$!: Observable<FaceSnap[]>;
  private destroy!: Subject<boolean>;

  constructor(private faceSnapService: FaceSnapService) {
  }

  ngOnInit() {
    this.destroy = new Subject<boolean>();
    this.faceSnaps$ = this.faceSnapService.getAllFaceSnaps();

    interval(1000).pipe(
      takeUntil(this.destroy),
      tap(value => console.log(value))
    ).subscribe();
  }

  ngOnDestroy() {
    this.destroy.next(true);
  }
}
